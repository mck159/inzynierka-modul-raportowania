package pl.edu.agh.msroka.pracaInzynierska.reportServer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.DTOConverter;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.ReportDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.EnumFrequency;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.Report;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.repositories.ReportRepository;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.config.ClientServicesConfig;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ReportService {
    @Autowired
    ReportRepository reportRepository;

    @Autowired
    DTOConverter dtoConverter;

    @Autowired
    ClientServicesConfig clientServicesConfig;


    public void validateReport(ReportDTO reportDTO) {
        // methods throws runtime exceptions
        String serviceName = reportDTO.getServiceName();
        ClientServicesConfig.ClientServiceConfig clientServiceConfig = clientServicesConfig.findConfigForService(serviceName);
        String reportName = reportDTO.getReportName();
        clientServiceConfig.findReport(reportName);
    }

    public String addReport(ReportDTO reportDTO) {
        this.validateReport(reportDTO);
        Report report = reportRepository.save(dtoConverter.fromAddReportDTO(reportDTO));
        return report.getId();
    }
    public List<ReportDTO> findByFrequency(EnumFrequency enumFrequency) {
        List<ReportDTO> reportDTOs = new ArrayList<>();
        for(Report report : reportRepository.findByFrequency(enumFrequency)) {
            reportDTOs.add(dtoConverter.toAddReportDTO(report));
        }
        return reportDTOs;
    }

    public Report getReport(String id) {
        return reportRepository.findOneById(id);
    }

    public void deleteReport(String id) {
        reportRepository.delete(reportRepository.findOneById(id));
    }
}
