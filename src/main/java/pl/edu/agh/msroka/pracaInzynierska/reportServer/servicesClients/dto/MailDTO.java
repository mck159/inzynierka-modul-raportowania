package pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.dto;

import java.util.List;

public class MailDTO {
    String subject;
    List<String> recipients;
    String body;
    Boolean immediate;


    public MailDTO() {
    }

    public MailDTO(String subject, List<String> recipients, String body, Boolean immediate) {
        this.subject = subject;
        this.recipients = recipients;
        this.body = body;
        this.immediate = immediate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getImmediate() {
        return immediate;
    }

    public void setImmediate(Boolean immediate) {
        this.immediate = immediate;
    }
}
