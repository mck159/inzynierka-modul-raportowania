package pl.edu.agh.msroka.pracaInzynierska.reportServer.dto;

import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.Report;

@Component
public class DTOConverter {
    public Report fromAddReportDTO(ReportDTO reportDTO) {
        Report report = new Report();
        report.setServiceName(reportDTO.getServiceName());
        report.setReportName(reportDTO.getReportName());
        report.setResourceParams(reportDTO.getResourceParams());
        report.setResourceUrl(reportDTO.getResourceUrl());
        report.setSubject(reportDTO.getSubject());
        report.setFrequency(reportDTO.getFrequency());
        report.setMulti(reportDTO.getMulti());
        return report;
    }

    public ReportDTO toAddReportDTO(Report report) {
        ReportDTO reportDTO = new ReportDTO();
        reportDTO.setServiceName(report.getServiceName());
        reportDTO.setReportName(report.getReportName());
        reportDTO.setResourceParams(report.getResourceParams());
        reportDTO.setResourceUrl(report.getResourceUrl());
        reportDTO.setSubject(report.getSubject());
        reportDTO.setFrequency(report.getFrequency());
        reportDTO.setMulti(report.getMulti());
        return reportDTO;
    }
}
