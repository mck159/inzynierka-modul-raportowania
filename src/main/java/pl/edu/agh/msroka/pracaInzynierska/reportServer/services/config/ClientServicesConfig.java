package pl.edu.agh.msroka.pracaInzynierska.reportServer.services.config;

import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.exceptions.NoConfigForReportFoundException;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.exceptions.NoConfigForServiceFoundException;

import java.util.List;

@Component
public class ClientServicesConfig {
    List<ClientServiceConfig> config;

    public ClientServicesConfig() {
    }

    public ClientServiceConfig findConfigForService(String serviceName) {
        for(ClientServiceConfig conf : config) {
            if(conf.getName().equals(serviceName)) {
                return conf;
            }
        }
        throw new NoConfigForServiceFoundException(serviceName);
    }

    public List<ClientServiceConfig> getConfig() {
        return config;
    }

    public void setConfig(List<ClientServiceConfig> config) {
        this.config = config;
    }

    public class ClientServiceConfig {
        public ReportConfig findReport(String reportName) {
            for(ReportConfig report : reports) {
                if(report.getName().equals(reportName)) {
                    return report;
                }
            }
            throw new NoConfigForReportFoundException(reportName);
        }
        String name;
        String url;
        List<ReportConfig> reports;

        public ClientServiceConfig() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public List<ReportConfig> getReports() {
            return reports;
        }

        public void setReports(List<ReportConfig> reports) {
            this.reports = reports;
        }

        public class ReportConfig {
            String name;
            String template;

            public ReportConfig() {
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTemplate() {
                return template;
            }

            public void setTemplate(String template) {
                this.template = template;
            }
        }
    }
}
