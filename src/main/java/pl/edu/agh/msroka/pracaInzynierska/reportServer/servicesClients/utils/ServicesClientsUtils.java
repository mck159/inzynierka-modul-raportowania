package pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.utils;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ServicesClientsUtils {
    public String prepareUrl(String url, Map<String, Object> params) {
        String newUrl = url;
        for(Map.Entry<String, Object> paramEntrySet : params.entrySet()) {
            String key = paramEntrySet.getKey();
            Object value = paramEntrySet.getValue();
            newUrl = newUrl.replaceAll("\\{" + key + "\\}", value.toString());
        }
        return newUrl;
    }
}
