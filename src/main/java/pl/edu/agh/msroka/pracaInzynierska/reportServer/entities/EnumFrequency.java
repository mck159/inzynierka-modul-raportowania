package pl.edu.agh.msroka.pracaInzynierska.reportServer.entities;

/**
 * Created by maciek on 26.11.15.
 */
public enum EnumFrequency {
    DAILY, WEEKLY, MONTHLY
}
