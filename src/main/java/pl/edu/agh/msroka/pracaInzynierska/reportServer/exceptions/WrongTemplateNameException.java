package pl.edu.agh.msroka.pracaInzynierska.reportServer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class WrongTemplateNameException extends RuntimeException {
    public WrongTemplateNameException(String tempateName) {
        super(String.format("No such template '%s'", tempateName));
    }
}
