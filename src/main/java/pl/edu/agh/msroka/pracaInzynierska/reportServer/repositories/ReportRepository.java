package pl.edu.agh.msroka.pracaInzynierska.reportServer.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.EnumFrequency;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.Report;

import java.util.List;

/**
 * Created by maciek on 26.11.15.
 */
public interface ReportRepository extends MongoRepository<Report, Integer> {
    public List<Report> findByFrequency(EnumFrequency enumFrequency);

    public Report findOneById(String id);
}
