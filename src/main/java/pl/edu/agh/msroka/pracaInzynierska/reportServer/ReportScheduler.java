package pl.edu.agh.msroka.pracaInzynierska.reportServer;

import com.mitchellbosecke.pebble.error.PebbleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.DataDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.ReportDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.EnumFrequency;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.ReportService;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.TemplatesService;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.config.ClientServicesConfig;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.DataServiceClient;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.MailingServiceClient;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.dto.MailDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.utils.ServicesClientsUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class ReportScheduler {
    @Autowired
    ReportService reportService;

    @Autowired
    TemplatesService templatesService;

    @Autowired
    MailingServiceClient mailingServiceClient;

    @Autowired
    DataServiceClient dataServiceClient;

    @Autowired
    ClientServicesConfig clientServicesConfig;

    @Autowired
    ServicesClientsUtils servicesClientsUtils;

    @Scheduled(cron = "*/10 * * * * *") // every 10 seconds
    public void sendFrequencyMinReports() throws IOException, PebbleException {
        List<ReportDTO> reportDTOs = reportService.findByFrequency(EnumFrequency.DAILY);
        for(ReportDTO reportDTO : reportDTOs) {
            ClientServicesConfig.ClientServiceConfig serviceConfig = clientServicesConfig.findConfigForService(reportDTO.getServiceName());
            ClientServicesConfig.ClientServiceConfig.ReportConfig reportConfig = serviceConfig.findReport(reportDTO.getReportName());
            String url = servicesClientsUtils.prepareUrl(String.format("%s/%s", serviceConfig.getUrl(), reportDTO.getResourceUrl()), (Map<String, Object>) reportDTO.getResourceParams());
            if(!reportDTO.getMulti()) {
                DataDTO data = dataServiceClient.getDataFromService(url, (Map<String, Object>) reportDTO.getResourceParams());
            } else {
                List<DataDTO> dataList= dataServiceClient.getMultiDataFromService(url, (Map<String, Object>) reportDTO.getResourceParams());
                for(DataDTO data : dataList) {
                    renderAndSend(data, reportDTO, reportConfig);
                }
            }
        }
    }
    @Scheduled(cron = "* */1 * * * *") // every minute
    public void sendFrequencyOftenReports() {
    }
    @Scheduled(cron = "* * */1 * * *") // every hour
    public void sendFrequencyMaxReports() {
    }

    public void renderAndSend(DataDTO data, ReportDTO reportDTO, ClientServicesConfig.ClientServiceConfig.ReportConfig reportConfig) throws IOException, PebbleException {
        String rendered = templatesService.renderTemplate(reportConfig.getTemplate(), data.getData());
        //mailingServiceClient.sendEmail(new MailDTO(reportDTO.getSubject(), data.getRecipients(), rendered, true)); // TODO change immediate to false
    }

}
