package pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.crossstore.HashMapChangeSet;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.dto.MailDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.utils.ServicesClientsUtils;

import java.util.HashMap;

@Component
public class MailingServiceClient {
    @Autowired
    ServicesClientsUtils servicesClientsUtils;

    @Autowired
    RestTemplate restTemplate;

    @Value("${services.urls.mailing}")
    String url;

    public void sendEmail(MailDTO mailDTO) {
        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(url + "/mail", mailDTO, Object.class);
    }
}
