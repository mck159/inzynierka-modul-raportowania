package pl.edu.agh.msroka.pracaInzynierska.reportServer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoConfigForReportFoundException extends RuntimeException {
    public NoConfigForReportFoundException(String reportName) {
        super(reportName);
    }
}
