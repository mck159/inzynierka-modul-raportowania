package pl.edu.agh.msroka.pracaInzynierska.reportServer.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.EnumFrequency;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by maciek on 26.11.15.
 */
public class ReportDTO {
    @NotEmpty
    String reportName;
    @NotEmpty
    String serviceName;
    @NotEmpty
    String resourceUrl;
    @NotNull
    Object resourceParams;

    @NotEmpty
    String subject;

    @NotNull
    EnumFrequency frequency;

    Boolean multi;

    public ReportDTO() {
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public Object getResourceParams() {
        return resourceParams;
    }

    public void setResourceParams(Object resourceParams) {
        this.resourceParams = resourceParams;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public EnumFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(EnumFrequency frequency) {
        this.frequency = frequency;
    }

    public Boolean getMulti() {
        return multi;
    }

    public void setMulti(Boolean multi) {
        this.multi = multi;
    }
}
