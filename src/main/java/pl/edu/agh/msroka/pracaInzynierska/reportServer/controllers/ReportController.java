package pl.edu.agh.msroka.pracaInzynierska.reportServer.controllers;

import com.mitchellbosecke.pebble.error.PebbleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.DTOConverter;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.ReportDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.entities.Report;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.ReportService;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/report")
public class ReportController {
    @Autowired
    ReportService reportService;

    @Autowired
    DTOConverter dtoConverter;



    @RequestMapping(value="", method = RequestMethod.POST)
    @ResponseBody
    String addReport(@Valid @RequestBody ReportDTO reportDTO) throws PebbleException, IOException {
        return reportService.addReport(reportDTO);
    }
    @RequestMapping(value="", method = RequestMethod.GET)
    @ResponseBody
    ReportDTO getReport(@RequestParam("id") String id) throws PebbleException, IOException {
        Report report = reportService.getReport(id);
        return dtoConverter.toAddReportDTO(report);
    }

    @RequestMapping(value="", method = RequestMethod.DELETE)
    @ResponseBody
    void deleteReport(@RequestParam("id") String id) throws PebbleException, IOException {
        reportService.deleteReport(id);
    }
}
