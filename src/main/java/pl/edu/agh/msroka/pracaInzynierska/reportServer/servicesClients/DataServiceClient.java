package pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.dto.DataDTO;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.ReportService;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.servicesClients.utils.ServicesClientsUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class DataServiceClient {
    @Autowired
    ServicesClientsUtils servicesClientsUtils;

    @Autowired
    RestTemplate restTemplate;

    public DataDTO getDataFromService(String url, Map<String, Object> params) {
        return restTemplate.getForObject(url, DataDTO.class, params);
    }

    public List<DataDTO> getMultiDataFromService(String url, Map<String, Object> params) {
        DataDTO[] result = restTemplate.getForObject(url, DataDTO[].class, params);
        return
                Arrays.asList(result);
    }
}
