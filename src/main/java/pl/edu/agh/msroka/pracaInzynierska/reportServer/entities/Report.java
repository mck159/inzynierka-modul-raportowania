package pl.edu.agh.msroka.pracaInzynierska.reportServer.entities;

import org.springframework.data.annotation.Id;

import java.util.List;

public class Report {
    @Id
    String id;

    String reportName;
    String serviceName;
    String resourceUrl;
    Object resourceParams;

    String subject;

    EnumFrequency frequency;

    Boolean multi;

    public Report() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public Object getResourceParams() {
        return resourceParams;
    }

    public void setResourceParams(Object resourceParams) {
        this.resourceParams = resourceParams;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public EnumFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(EnumFrequency frequency) {
        this.frequency = frequency;
    }

    public Boolean getMulti() {
        return multi;
    }

    public void setMulti(Boolean multi) {
        this.multi = multi;
    }
}
