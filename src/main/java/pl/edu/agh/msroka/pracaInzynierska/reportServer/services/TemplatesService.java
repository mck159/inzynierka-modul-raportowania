package pl.edu.agh.msroka.pracaInzynierska.reportServer.services;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.loader.FileLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

@Service
public class TemplatesService {
    @Autowired
    PebbleEngine pebbleEngine;

    public String renderTemplate(String templateName, Object data) throws IOException, PebbleException {
        PebbleEngine pebbleEngine = new PebbleEngine(new FileLoader()); // TODO - delete to cache templates
        Writer writer = new StringWriter();
        PebbleTemplate pebbleTemplate = pebbleEngine.getTemplate(String.format("/home/maciek/tmp/templates/%s.peb", templateName));
        Map<String, Object> templateContext = new HashMap<>();
        templateContext.put("context", data);
        pebbleTemplate.evaluate(writer, templateContext);
        return writer.toString();
    }
}
