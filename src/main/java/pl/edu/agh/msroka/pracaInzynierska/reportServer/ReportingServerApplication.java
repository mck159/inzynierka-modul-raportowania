package pl.edu.agh.msroka.pracaInzynierska.reportServer;

import com.google.gson.Gson;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.FileLoader;
import com.mitchellbosecke.pebble.loader.Loader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.msroka.pracaInzynierska.reportServer.services.config.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

@SpringBootApplication
//@EnableScheduling
public class ReportingServerApplication {
    @Bean
    Loader loader() {return new FileLoader(); }

    @Bean
    PebbleEngine pebbleEngine(Loader loader) {
        return new PebbleEngine(loader);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    ClientServicesConfig clientServicesConfig() throws FileNotFoundException {
        Gson gson = new Gson();
        ClientServicesConfig.ClientServiceConfig[] clientServiceConfigs = gson.fromJson(new FileReader("services.json"), ClientServicesConfig.ClientServiceConfig[].class);
        ClientServicesConfig clientServicesConfig = new ClientServicesConfig();
        clientServicesConfig.setConfig(Arrays.asList(clientServiceConfigs));
        return clientServicesConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(ReportingServerApplication.class, args);
    }
}
