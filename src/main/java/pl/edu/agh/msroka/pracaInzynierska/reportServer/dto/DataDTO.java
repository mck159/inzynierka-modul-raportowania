package pl.edu.agh.msroka.pracaInzynierska.reportServer.dto;

import java.util.HashMap;
import java.util.List;

/**
 * Created by maciek on 29.11.15.
 */
public class DataDTO {
    List<String> recipients;
    HashMap<String,Object> data;

    public DataDTO() {
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }
}
